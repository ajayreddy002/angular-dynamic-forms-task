export interface IResponseModel {
    result: any;
    error: boolean;
    message: string;
    status_code: number;
}
