import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { IResponseModel } from '../_models/response.model';
import { AppConstants } from '../app.constants';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material';
import { ProductViewDailog } from '../dialogs/productview/productViewDialog';
import { HttpRequestService } from '../_httpservices/httpRequestSrvice';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  addProductForm: FormGroup;
  productList: any[];
  product: any;
  isProduct: boolean;
  isEdit: boolean;
  button_text = 'Submit';
  productId: any;
  backBtn = false;
  inputFields: any = [];
  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private httpRequestService: HttpRequestService
  ) {}

  async ngOnInit() {
    this.inputFields = await this.getFormFields();
    const group: any = {};
    this.inputFields.map(item => {
      group[item.name] = item.mandatory
        ? new FormControl(item.defaultValue || '', Validators.required)
        : new FormControl(item.defaultValue || '');
    });
    this.addProductForm = new FormGroup(group);
    console.log(this.addProductForm.controls.description.value);
    this.getProducts();
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
  getFormFields = () => {
    return new Promise((resolve, reject) => {
      this.httpClient
        .get(AppConstants.AUTHENTICATION_URL + 'getTvFormFields')
        .subscribe(
          field => {
            resolve(field);
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }
  submitaddProductForm = () => {
    // This is to update the product
    if (this.productId) {
      this.addProductForm.controls.id.setValue(this.productId);
      const formModel = this.addProductForm.value;
      this.httpRequestService.updateRequest(formModel).subscribe(
        data => {
          this.isEdit = false;
          this.isProduct = false;
          if (!data.error && data.status_code === 100) {
            this.openSnackBar(data.message, 'close');
            this.getProducts();
          } else {
            this.openSnackBar(data.message, 'close');
          }
        },
        err => {
          this.openSnackBar('Something went wrong', 'close');
        }
      );
    } else {
      // This is to add the product
      const formModel = this.addProductForm.value;
      this.httpRequestService.postRequest(formModel).subscribe(
        data => {
          this.isEdit = false;
          this.isProduct = false;
          if (!data.error && data.status_code === 100) {
            this.openSnackBar(data.message, 'close');
            this.getProducts();
          } else {
            this.openSnackBar(data.message, 'close');
          }
        },
        err => {
          this.openSnackBar('Something went wrong', 'close');
        }
      );
    }
  }
  // Get entire data
  getProducts = () => {
    this.httpRequestService.getRequest().subscribe(
      data => {
        if (!data.error && data.result.length > 0) {
          this.productList = data.result;
          console.log(this.productList);
        }
      },
      err => {
        this.openSnackBar('Something went wrong', 'close');
      }
    );
  }
  // Get specific product
  getProductById = id => {
    this.isProduct = true;
    this.httpClient
      .get<IResponseModel>(
        AppConstants.AUTHENTICATION_URL + `get_products/${id}`
      )
      .subscribe(
        data => {
          if (!data.error && data.result.length > 0) {
            this.product = data.result;
            const dialogRef = this.dialog.open(ProductViewDailog, {
              width: '500px',
              data: this.product
            });
            dialogRef.afterClosed().subscribe(result => {
              console.log('The dialog was closed');
            });
          } else {
            this.openSnackBar(data.message, 'close');
          }
        },
        err => {
          this.openSnackBar('Something went wrong', 'close');
        }
      );
  }
  gotoProduct = () => {
    this.isEdit = false;
    this.backBtn = false;
  }
  // This is for edit product
  editProduct = productData => {
    this.addProductForm.reset();
    this.setDefaultProductData(productData);
    this.isEdit = true;
    this.button_text = 'Update';
    this.backBtn = true;
  }
  // This is for add product
  addProduct = () => {
    this.isEdit = true;
    this.button_text = 'Submit';
    this.backBtn = false;
  }
  // This is for set default values when click on edit
  setDefaultProductData = productData => {
    if (productData) {
      this.addProductForm.controls.name.setValue(productData.name);
      this.addProductForm.controls.description.setValue(
        productData.description
      );
      this.addProductForm.controls.height.setValue(productData.height);
      this.addProductForm.controls.width.setValue(productData.width);
      this.addProductForm.controls.is_smart.setValue(productData.is_smart);
      this.productId = productData.id;
    }
  }
}
