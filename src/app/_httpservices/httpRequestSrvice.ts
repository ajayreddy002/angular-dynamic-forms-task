import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IResponseModel } from '../_models/response.model';
import { AppConstants } from '../app.constants';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class HttpRequestService {
    constructor(private httpClient: HttpClient) {}
    postRequest = (data: any): Observable<any> => {
        return this.httpClient.post<IResponseModel>(AppConstants.AUTHENTICATION_URL + 'addCategory', data);
    }
    updateRequest = (data: any): Observable<any> => {
        return this.httpClient.put<IResponseModel>(AppConstants.AUTHENTICATION_URL + 'addCategory', data);
    }
    getRequest = (): Observable<any> => {
        return this.httpClient.get(AppConstants.AUTHENTICATION_URL + 'get_products');
    }
}
