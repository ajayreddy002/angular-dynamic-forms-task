import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    templateUrl: 'productViewDialog.html',
  })
  // tslint:disable-next-line:component-class-suffix
  export class ProductViewDailog {
    constructor(
      public dialogRef: MatDialogRef<ProductViewDailog>,
      @Inject(MAT_DIALOG_DATA) public data: any) {}
    onNoClick(): void {
      this.dialogRef.close();
    }
  }
