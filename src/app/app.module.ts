import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSnackBarModule, MatDialogModule} from '@angular/material';
import { ProductViewDailog } from './dialogs/productview/productViewDialog';
import { HttpRequestService } from './_httpservices/httpRequestSrvice';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductViewDailog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  providers: [HttpRequestService],
  bootstrap: [AppComponent],
  entryComponents: [ProductViewDailog]
})
export class AppModule { }
